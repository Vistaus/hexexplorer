/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * HexExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.2 as QC
import Ubuntu.Components 1.3

Page {
    id: settingsPage
    anchors.fill: parent

    /* defining main slider values as properties */

    // Background Values
    property int backgroundRedValue: backgroundRedSlider.value
    property int backgroundGreenValue: backgroundGreenSlider.value
    property int backgroundBlueValue: backgroundBlueSlider.value

    property string backgroundRedString: (backgroundRedValue < 16)  ? "0" + backgroundRedValue.toString(16) : backgroundRedValue.toString(16)
    property string backgroundGreenString: (backgroundGreenValue < 16)  ? "0" + backgroundGreenValue.toString(16) : backgroundGreenValue.toString(16)
    property string backgroundBlueString: (backgroundBlueValue < 16)  ? "0" + backgroundBlueValue.toString(16) : backgroundBlueValue.toString(16)

    property string newBackgroundColour: "#" + backgroundRedString + backgroundGreenString + backgroundBlueString

    // Ink Values
    property int inkRedValue: inkRedSlider.value
    property int inkGreenValue: inkGreenSlider.value
    property int inkBlueValue: inkBlueSlider.value

    property string inkRedString: (inkRedValue < 16) ? "0" + inkRedValue.toString(16): inkRedValue.toString(16)
    property string inkGreenString: (inkGreenValue < 16) ? "0" + inkGreenValue.toString(16): inkGreenValue.toString(16)
    property string inkBlueString: (inkBlueValue < 16) ? "0" + inkBlueValue.toString(16): inkBlueValue.toString(16)

    property string newInkColour: "#" + inkRedString + inkGreenString + inkBlueString

    // Default Background and Ink Values
    property int defaultBackgroundRed: 0x2c
    property int defaultBackgroundGreen: 0x00
    property int defaultBackgroundBlue: 0x1e

    property int defaultInkRed: 0xe9
    property int defaultInkGreen: 0x54
    property int defaultInkBlue: 0x20

    /* end of defining main slider values as properties */

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')

        StyleHints {
            foregroundColor: appSettings.persistentInkColor
            backgroundColor: appSettings.persistentBackgroundColor
            dividerColor: Qt.darker(backgroundColour)
        }
    }

    Component.onCompleted: {
        backgroundRedSlider.value = appSettings.persistentBackgroundRedValue
        backgroundGreenSlider.value = appSettings.persistentBackgroundGreenValue
        backgroundBlueSlider.value = appSettings.persistentBackgroundBlueValue

        inkRedSlider.value = appSettings.persistentInkRedValue
        inkGreenSlider.value = appSettings.persistentInkGreenValue
        inkBlueSlider.value = appSettings.persistentInkBlueValue
    }

    Rectangle {
        id: quickChangeSection
        width: parent.width
        height: parent.height/11
        color: appSettings.persistentBackgroundColor
        anchors.top: header.bottom

        Rectangle {
            id: hexRectangle
            width: parent.width - units.gu(2)
            height: parent.height - units.gu(2)
            anchors.centerIn: parent
            color: newBackgroundColour
            radius: 8

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    appSettings.persistentInkColor = newInkColour
                    root.backgroundColor = newBackgroundColour
                    appSettings.persistentBackgroundColor = newBackgroundColour
                    inkColour = newInkColour
                    appSettings.persistentBackgroundRedValue = backgroundRedSlider.value
                    appSettings.persistentBackgroundGreenValue = backgroundGreenSlider.value
                    appSettings.persistentBackgroundBlueValue = backgroundBlueSlider.value
                    appSettings.persistentInkRedValue = inkRedSlider.value
                    appSettings.persistentInkGreenValue = inkGreenSlider.value
                    appSettings.persistentInkBlueValue = inkBlueSlider.value
                }

        } //MouseArea

        } //hexRectangle

        Label {
            id: changeLabel
            text: i18n.tr("Set background to %1").arg(newBackgroundColour) +"\n" + i18n.tr("Set text to %1").arg(newInkColour)
            anchors.centerIn: parent
            width: parent.width - units.gu(4)
            wrapMode: Text.WordWrap
            color: newInkColour
            font.pixelSize: appSettings.standardFontSize
            horizontalAlignment: Text.AlignHCenter
        }  // changeLabel

        // Rectangle {
        //     id: swapColorsIcon
        //     anchors {
        //         right: parent.right
        //         rightMargin: units.gu(2)
        //         verticalCenter: parent.verticalCenter
        //     }
        //     height: units.gu(3)
        //     width: height
        //     color: swapClick.pressed ? appSettings.persistentInkColor : "transparent"
        //     MouseArea {
        //         id: swapClick
        //         anchors.fill: parent
        //         onClicked: {
        //             //TODO: swap colors of ink and background, sliderink -> sliderbackground and vice versa
        //         }
        //     }
        //     Icon {
        //         height: parent.height * 0.75
        //         anchors.centerIn: parent
        //         color: appSettings.persistentInkColor
        //         width: height
        //         name: "user-switch"
        //     }
        // }
    } //quickChangeSection

    Rectangle {
        id: topSection
        width: parent.width
        height: (parent.height - (header.height + quickChangeSection.height)) /2
        anchors.top: quickChangeSection.bottom
        color: appSettings.persistentBackgroundColor

        Rectangle {
            id: page1TopHeading
            width: parent.width
            height: parent.height/7
            anchors.top: parent.top
            color: appSettings.persistentBackgroundColor // theme.palette.normal.background

            Rectangle {
                id: topHeadingSpacerTop
                width: parent.width
                height: 1
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //topHeadingSpacerTop

            Label {
                text: i18n.tr("Background")
                color: appSettings.persistentInkColor // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
            } //Label

            Label {
                text: newBackgroundColour
                color: appSettings.persistentInkColor  // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.centerIn: parent
            } //Label


            Rectangle {
                id: topHeadingSpacerBottom
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //topHeadingSpacerBottom


        } //page1TopHeading


        Rectangle {
            id: backgroundRedSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height) / 3
            anchors.top: page1TopHeading.bottom
            color: parent.color

            QC.Slider {
                id: backgroundRedSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultBackgroundRed
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentBackgroundRedValue = value
            } //backgroundRedSlider

        } //backgroundRedSliderSection

        Rectangle {
            id: backgroundGreenSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height) / 3
            anchors.top: backgroundRedSliderSection.bottom
            color: parent.color

            QC.Slider {
                id: backgroundGreenSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultBackgroundGreen
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentBackgroundGreenValue = value
            } //backgroundGreenSlider

        } //backgroundGreenSliderSection

        Rectangle {
            id: backgroundBlueSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height) / 3
            anchors.bottom: parent.bottom
            color: parent.color

            QC.Slider {
                id: backgroundBlueSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultBackgroundBlue
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentBackgroundBlueValue = value
            } //backgroundBlueSlider

        } //backgroundBlueSliderSection

        Rectangle {
            id: topSectionSpacer
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: "transparent"
            border.color: Qt.darker(backgroundColour)
        } //topSectionSpacer

    } //topSection

    Rectangle {
        id: bottomSection
        width: parent.width
        height: (parent.height - (header.height + quickChangeSection.height)) /2
        anchors.bottom: parent.bottom
        color: appSettings.persistentBackgroundColor

        Rectangle {
            id: page1BottomHeading
            width: parent.width
            height: parent.height/7
            anchors.top: parent.top
            color: appSettings.persistentBackgroundColor  // theme.palette.normal.background

            Label {
                id: bottomHeadingLabel
                text: i18n.tr("Text")
                color: appSettings.persistentInkColor  // theme.palette.normal.baseText
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
                font.pixelSize: appSettings.standardFontSize
            } //bottomHeadingLabel

            Label {
                id: bottomHeadingHex
                text: newInkColour
                color: appSettings.persistentInkColor // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.centerIn: parent
            } //bottomHeadingHex

            Rectangle {
                id: page1BottomHeadingSpacer
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //page1BottomHeadingSpacer

        } //page1BottomHeading

        Rectangle {
            id: inkRedSliderSection
            width: parent.width
            height: parent.height/7 * 2
            anchors.top: page1BottomHeading.bottom
            color: parent.color

            QC.Slider {
                id: inkRedSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultInkRed
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentInkRedValue = value
            } //inkRedSlider

        } //inkRedSliderSection

        Rectangle {
            id: inkGreenSliderSection
            width: parent.width
            height: parent.height/7 * 2
            anchors.top: inkRedSliderSection.bottom
            color: parent.color

            QC.Slider {
                id: inkGreenSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultInkGreen
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentInkGreenValue = value
            } //inkGreenSlider

        } //inkGreenSliderSection

        Rectangle {
            id: inkBlueSliderSection
            width: parent.width
            height: parent.height/7 * 2
            anchors.bottom: parent.bottom
            color: parent.color

            QC.Slider {
                id: inkBlueSlider
                width: parent.width/5 * 4
                handle.height: appSettings.standardFontSize
                handle.width: handle.height
                anchors.centerIn: parent
                from: 0x00
                value: defaultInkBlue
                to: 0xff
                stepSize: 0x01
                onMoved: appSettings.persistentInkBlueValue = value
            } //inkBlueSlider

        } //inkBlueSliderSection

    } //bottomSection
}
