v2.0.0.
- add: migrate the app to Gitlab
- add: new maintainer Daniel Danfro Frost
- add: allow copy of hex code
- add: about page
- add: German translation
- fix: made the app arch all instead of being architecture dependend by removing c++ code
- fix: settings behavior
- fix: use UT style header and icons
